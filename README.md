## Author: Michael O'Connell, moconnel@uoregon.edu ##

Description:
This project extends a tiny web server in Python to check understanding of basic web architecture. Extensions include: (1) checking for errors in client's server requests (404 & 403) and (2) displaying a webpage from a valid request.
